FROM node

WORKDIR /usr/src/app

COPY ./yarn.lock .
COPY ./package.json .
COPY ./tsconfig.json .
COPY ./next-env.d.ts .
COPY ./next.config.js .

COPY ./src ./src
COPY ./public ./public

RUN yarn install
RUN yarn build

EXPOSE 3000

CMD [ "yarn", "start" ]
