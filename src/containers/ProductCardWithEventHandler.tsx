import * as React from 'react';
import ProductCard, { IProductCardProps } from '../share/components/ProductCard';
import storage from '../storage';
import { useRouter } from 'next/router';
import URLS from '../statics/URLS';
import make from '../share/functions/make';

export interface IProductCardWithEventHandlerProps extends IProductCardProps {
    id?: string;
}

export default function ProductCardWithEventHandler(props: IProductCardWithEventHandlerProps) {
    const { id } = props;

    const router = useRouter();
    const { store, dispatchStore } = storage.cart.useCartStore();

    const handlerBuyNowButtonClick =
        props.onBuyNowButtonClick ||
        (() => {
            if (!id) {
                console.warn('id of ProductCard undefined');
                return;
            }
            dispatchStore(storage.cart.actions.addToCart(id));
            router.push(URLS.cart);
        });

    const handlerAddToCartButtonClick =
        props.onAddToCartButtonClick ||
        (() => {
            if (!id) {
                console.warn('id of ProductCard undefined');
                return;
            }
            dispatchStore(storage.cart.actions.addToCart(id));
        });

    const handlerDecreaseQuantityButtonClick =
        props.onAddToCartButtonClick ||
        (() => {
            if (!id) {
                console.warn('id of ProductCard undefined');
                return;
            }
            dispatchStore(storage.cart.actions.removeFromCart(id));
        });

    const handlerIncreaseQuantityButtonClick = handlerAddToCartButtonClick;

    const handlerRemoveButtonClick =
        props.onAddToCartButtonClick ||
        (() => {
            if (!id) {
                console.warn('id of ProductCard undefined');
                return;
            }
            // dispatch quantity = 0 is remove item from cart
            dispatchStore(storage.cart.actions.changeQuantity(id, 0));
        });

    return (
        <ProductCard
            className={props.className}
            slug={make.linkProduct(props.slug)}
            image={props.image}
            title={props.title}
            isPromotionalPrice={props.isPromotionalPrice}
            price={props.price}
            promotionPrice={props.promotionPrice}
            summary={props.summary}
            didAddedToCart={props.id !== undefined && store.data.has(props.id)}
            quantityAdded={props.id !== undefined ? store.data.get(props.id)?.quantity : undefined}
            onBuyNowButtonClick={handlerBuyNowButtonClick}
            onAddToCartButtonClick={handlerAddToCartButtonClick}
            onIncreaseQuantityButtonClick={handlerIncreaseQuantityButtonClick}
            onDecreaseQuantityButtonClick={handlerDecreaseQuantityButtonClick}
            onRemoveClick={handlerRemoveButtonClick}
        />
    );
}
