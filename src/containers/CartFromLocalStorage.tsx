import * as React from 'react';
import LapisLocalStorage from '../core/LapisLocalStorage';
import storage from '../storage';

export interface ICartFromLocalStorageProps {}

export default function CartFromLocalStorage(props: React.PropsWithChildren<ICartFromLocalStorageProps>) {
    const { dispatchStore } = storage.cart.useCartStore();

    React.useEffect(() => {
        if (LapisLocalStorage.instance.didLoadCart) return;
        dispatchStore(storage.cart.actions.loadCart());
        console.log('đã load');
    }, [dispatchStore]);

    return <>{props.children}</>;
}
