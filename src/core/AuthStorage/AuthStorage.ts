interface IAuthStorageData {
    accessToken: string;
    refreshToken: string;
}

class AuthStorage {
    private keyName: string = 'AuthStorage';
    private _instance: AuthStorage | undefined = undefined;
    public get instance() {
        if (!this._instance) this._instance = new AuthStorage();
        return this._instance;
    }

    private _data: IAuthStorageData = {
        accessToken: '',
        refreshToken: '',
    };

    // access token
    public get accessToken() {
        return this._data.accessToken;
    }

    public set accessToken(v: string) {
        this._data.accessToken = v;
        this.save();
    }

    // refresh token
    public get refreshToken() {
        return this._data.refreshToken;
    }

    public set refreshToken(v: string) {
        this._data.refreshToken = v;
        this.save();
    }

    protected constructor() {
        this.loadData();
    }

    private save = () => {
        window.localStorage.setItem(this.keyName, JSON.stringify(this._data));
    };

    private loadData = () => {
        if (typeof window === 'undefined') return;
        const strData = window.localStorage.getItem('auth');

        if (!strData) {
            this.save();
            return;
        }

        try {
            const objData = JSON.parse(strData);
            const { accessToken, refreshToken } = objData;

            if (typeof accessToken !== 'string' || typeof refreshToken !== 'string') {
                this.save();
                return;
            }

            this._data = {
                accessToken,
                refreshToken,
            };
            return;
        } catch {
            this.save();
            return;
        }
    };
}

export default AuthStorage;
