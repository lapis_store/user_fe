import { ICartItem } from '../storage/cart/reducer';

enum ELocalStorageItems {
    cart = 'cart',
}

export interface ICartItemLocal extends ICartItem {
    id: string;
}

export default class LapisLocalStorage {
    private static _instance: LapisLocalStorage | undefined = undefined;
    private _cart: ICartItemLocal[] | undefined = undefined;

    private constructor() {}

    public static get instance() {
        if (this._instance === undefined) {
            this._instance = new LapisLocalStorage();
        }
        return this._instance;
    }

    private readonly loadCart = (): ICartItemLocal[] => {
        if (typeof window === 'undefined') return [];

        const saveDefault = () => {
            localStorage.setItem(ELocalStorageItems.cart, '[]');
        };

        const strCart = localStorage.getItem('cart');

        if (!strCart) {
            saveDefault();
            return [];
        }

        try {
            const cart = JSON.parse(strCart);

            if (!Array.isArray(cart)) {
                saveDefault();
                return [];
            }

            return cart;
        } catch {
            saveDefault();
            return [];
        }
    };

    public get cart(): Map<string, ICartItem> {
        if (typeof window === 'undefined') return new Map();

        if (!this._cart) {
            this._cart = this.loadCart();
        }

        return this._cart.reduce((previousValue, item) => {
            previousValue.set(item.id, {
                createdAt: new Date(item.createdAt),
                quantity: item.quantity,
            });

            return previousValue;
        }, new Map<string, ICartItem>());
    }

    public set cart(v: Map<string, ICartItem>) {
        if (typeof window === undefined) return;

        this._cart = [];
        for (let [key, value] of v) {
            this._cart.push({
                id: key,
                quantity: value.quantity,
                createdAt: value.createdAt,
            });
        }

        // save
        localStorage.setItem(ELocalStorageItems.cart, JSON.stringify(this._cart));
    }

    public get didLoadCart(): boolean {
        return this._cart !== undefined;
    }
}
