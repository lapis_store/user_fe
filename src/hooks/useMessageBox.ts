import { IMessageBox } from '../share/components/MessageBox';
import storage from '../storage';

function useMessageBox() {
    const { store, dispatchStore, storeActions } = storage.root.useRootStore();

    return {
        show: (v: IMessageBox) => {
            dispatchStore(storeActions.addMessageAlert(v));
        },
    };
}

export default useMessageBox;
