export default function displayState<TState, TAction, T extends (a: TState, b: TAction) => TState>(
    name: string,
    reducer: T,
) {
    return (preState: TState, action: TAction): TState => {
        console.group(name);

        console.log('[PRE_STATE] ', { ...preState });
        const newState = reducer(preState, action);
        console.log('[NEW_STATE] ', newState);

        console.groupEnd();
        return newState;
    };
}
