import displayState from './displayState';

const middleware = {
    displayState,
};

export default middleware;
