import cart from './cart';
import middleware from './middleware';
import root from './root';

const storage = {
    middleware,
    cart,
    root,
};

export default storage;
