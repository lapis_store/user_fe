export enum EAction {
    addToCart = 'addToCart',
    removeFromCart = 'removeFromCart',
    loadCart = 'loadCart',
    changeQuantity = 'changeQuantity',
    removeAll = 'removeAll',
}

export interface IAction {
    type: EAction;
    payload: any;
}

const addToCart = (id: string): IAction => {
    return {
        type: EAction.addToCart,
        payload: {
            id,
        },
    };
};

const removeFromCart = (id: string): IAction => {
    return {
        type: EAction.removeFromCart,
        payload: {
            id,
        },
    };
};

const loadCart = (): IAction => {
    return {
        type: EAction.loadCart,
        payload: undefined,
    };
};

const changeQuantity = (id: string, quantity: number): IAction => {
    return {
        type: EAction.changeQuantity,
        payload: {
            id,
            quantity,
        },
    };
};

const removeAll = (): IAction => {
    return {
        type: EAction.removeAll,
        payload: undefined,
    };
};

const actions = {
    addToCart,
    removeFromCart,
    loadCart,
    changeQuantity,
    removeAll,
};

export default actions;
