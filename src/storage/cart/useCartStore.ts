import { useContext } from 'react';
import actions from './actions';
import context from './context';

export default function useCartStore() {
    // const {store, dispatchStore} = useContext(context);
    return {
        ...useContext(context),
        actions: actions,
    };
}
