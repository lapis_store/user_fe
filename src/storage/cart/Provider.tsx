import * as React from 'react';
import displayState from '../middleware/displayState';
import { IAction } from './actions';
import context from './context';
import reducer, { initState, TState } from './reducer';

export interface IProviderProps {}

export default function Provider(props: React.PropsWithChildren<IProviderProps>) {
    const [store, dispatchStore] = React.useReducer(
        displayState<TState, IAction, typeof reducer>('CART_STORE', reducer),
        initState,
    );

    // const [store, dispatchStore] = React.useReducer(
    //     reducer,
    //     initState
    // );

    return <context.Provider value={{ store, dispatchStore }}>{props.children}</context.Provider>;
}
