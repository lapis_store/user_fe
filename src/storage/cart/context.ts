import * as React from 'react';
import { IAction } from './actions';
import { TState } from './reducer';

const context = React.createContext<{
    store: TState;
    dispatchStore: React.Dispatch<IAction>;
}>({} as any);

export default context;
