import LapisLocalStorage from '../../core/LapisLocalStorage';
import { EAction, IAction } from './actions';
import handler from './handler';

export interface ICartItem {
    createdAt: Date;
    quantity: number;
}

export type TState = {
    data: Map<string, ICartItem>;
};

export const initState: TState = {
    data: new Map(),
};

const reducer = (preState: TState, action: IAction): TState => {
    switch (action.type) {
        case EAction.addToCart: {
            return handler.addToCart(preState, action.payload);
        }
        case EAction.removeFromCart: {
            return handler.removeFromCart(preState, action.payload);
        }
        case EAction.loadCart: {
            return handler.loadCart(preState, action.payload);
        }
        case EAction.changeQuantity: {
            return handler.changeQuantity(preState, action.payload);
        }
        case EAction.removeAll: {
            return handler.removeAll(preState, action.payload);
        }
        default: {
            console.warn('[reducer of cart storage] Invalid type of action');
        }
    }
    return preState;
};

export default reducer;
