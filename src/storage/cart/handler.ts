import LapisLocalStorage from '../../core/LapisLocalStorage';
import { TState } from './reducer';

const addToCart = (preState: TState, payload: { id: string }): TState => {
    const { id } = payload;

    const item = preState.data.get(id);
    const createdAt = item?.createdAt || new Date();
    const quantity = (item?.quantity || 0) + 1;

    const data = new Map(preState.data);
    data.set(id, {
        createdAt,
        quantity,
    });

    LapisLocalStorage.instance.cart = data;
    return {
        ...preState,
        data,
    };
};

const removeFromCart = (preState: TState, payload: { id: string }): TState => {
    const { id } = payload;

    const item = preState.data.get(id);
    const createdAt = item?.createdAt || new Date();
    const quantity = (item?.quantity || 0) - 1;

    const data = new Map(preState.data);

    if (quantity <= 0) {
        data.delete(id);
    } else {
        data.set(id, {
            createdAt,
            quantity,
        });
    }

    LapisLocalStorage.instance.cart = data;
    return {
        ...preState,
        data,
    };
};

const changeQuantity = (preState: TState, payload: { id: string; quantity: number }): TState => {
    const { id, quantity } = payload;

    const item = preState.data.get(id);
    const createdAt = item?.createdAt || new Date();

    const data = new Map(preState.data);
    if (quantity <= 0) {
        data.delete(id);
    } else {
        data.set(id, {
            createdAt,
            quantity,
        });
    }

    LapisLocalStorage.instance.cart = data;
    return {
        ...preState,
        data,
    };
};

const loadCart = (preState: TState, payload: any): TState => {
    return {
        ...preState,
        data: LapisLocalStorage.instance.cart,
    };
};

const removeAll = (preState: TState, payload: any): TState => {
    LapisLocalStorage.instance.cart = new Map();

    return {
        ...preState,
        data: LapisLocalStorage.instance.cart,
    };
};

const handler = {
    addToCart,
    removeFromCart,
    loadCart,
    changeQuantity,
    removeAll,
};

export default handler;
