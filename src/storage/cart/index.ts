import actions from './actions';
import Provider from './Provider';
import useCartStore from './useCartStore';

const cart = {
    useCartStore,
    actions,
    Provider,
};

export default cart;
