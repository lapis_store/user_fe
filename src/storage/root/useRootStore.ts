import * as React from 'react';
import { Context } from './Provider';

function useRootStore() {
    // const {store, dispatchStore, storeActions} = React.useContext(Context);
    return React.useContext(Context);
}

export default useRootStore;
