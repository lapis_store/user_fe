import actions from './actions';
import Provider from './Provider';
import useRootStore from './useRootStore';

const root = {
    actions,
    Provider,
    useRootStore,
};

export default root;
