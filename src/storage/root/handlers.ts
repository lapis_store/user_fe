import { IMessageBox } from '../../share/components/MessageBox';
import { TState } from './reducer';

const addMessageAlert = (preState: TState, payload: IMessageBox): TState => {
    const { messageBoxData, counter } = preState;

    const newMessageBoxData = new Map(messageBoxData);
    newMessageBoxData.set(counter, payload);

    return {
        ...preState,
        messageBoxData: newMessageBoxData,
        counter: counter + 1,
    };
};

const removeMessageAlert = (preState: TState, payload: number): TState => {
    const { messageBoxData, counter } = preState;

    const newMessageBoxData = new Map(messageBoxData);
    newMessageBoxData.delete(payload);

    return {
        ...preState,
        messageBoxData: newMessageBoxData,
    };
};

const setDisplayCategory = (preState: TState, payload: boolean): TState => {
    return {
        ...preState,
        displayCategory: payload,
    };
};

const handlers = {
    addMessageAlert,
    removeMessageAlert,
    setDisplayCategory,
};

export default handlers;
