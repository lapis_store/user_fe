import { IMessageBox } from '../../share/components/MessageBox';
import { EActions, TAction } from './actions';
import handlers from './handlers';

export type TState = {
    counter: number;
    messageBoxData: Map<number, IMessageBox>;
    displayCategory: boolean;
};

export const initState: TState = {
    counter: 0,
    messageBoxData: new Map(),
    displayCategory: false,
};

const reducer = (preState: TState, action: TAction) => {
    const { type, payload } = action;

    switch (type) {
        case EActions.addMessageAlert: {
            return handlers.addMessageAlert(preState, payload);
        }
        case EActions.removeMessageAlert: {
            return handlers.removeMessageAlert(preState, payload);
        }
        case EActions.setDisplayCategory: {
            return handlers.setDisplayCategory(preState, payload);
        }
        default: {
            console.error(`[ROOT STORE] Invalid action type: ${type}`);
            return preState;
        }
    }
};

export default reducer;
