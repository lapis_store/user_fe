import * as React from 'react';
import displayState from '../middleware/displayState';
import actions, { TAction } from './actions';
import reducer, { initState, TState } from './reducer';

export const Context = React.createContext<{
    store: TState;
    dispatchStore: React.Dispatch<TAction>;
    storeActions: typeof actions;
}>({} as any);

export interface IProviderProps {}

export default function Provider(props: React.PropsWithChildren<IProviderProps>) {
    const [store, dispatchStore] = React.useReducer(
        displayState<TState, TAction, typeof reducer>('[ROOT_STORE]', reducer),
        initState,
    );

    // const [store, dispatchStore] = React.useReducer(
    //     reducer,
    //     initState
    // );

    return (
        <Context.Provider
            value={{
                store,
                dispatchStore,
                storeActions: actions,
            }}
        >
            {props.children}
        </Context.Provider>
    );
}
