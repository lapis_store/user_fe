import { IMessageBox } from '../../share/components/MessageBox';

export enum EActions {
    addMessageAlert = 'addMessageAlert',
    removeMessageAlert = 'removeMessageAlert',
    setDisplayCategory = 'setDisplayCategory',
}

export type TAction = {
    type: EActions;
    payload: any;
};

const setDisplayCategory = (v: boolean): TAction => {
    return {
        type: EActions.setDisplayCategory,
        payload: v,
    };
};

const addMessageAlert = (v: IMessageBox): TAction => {
    return {
        type: EActions.addMessageAlert,
        payload: v,
    };
};

const removeMessageAlert = (id: number): TAction => {
    return {
        type: EActions.removeMessageAlert,
        payload: id,
    };
};

const actions = {
    addMessageAlert,
    removeMessageAlert,
    setDisplayCategory,
};

export default actions;
