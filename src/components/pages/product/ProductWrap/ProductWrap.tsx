import { useRouter } from 'next/router';
import * as React from 'react';
import useMessageBox from '../../../../hooks/useMessageBox';
import Article from '../../../../share/components/Article';
import ImagesProductSlider from '../../../../share/components/ImagesProductSlider';
import ProductDetail from '../../../../share/components/ProductDetail';
import ProductSpecifications from '../../../../share/components/ProductSpecifications';
import make from '../../../../share/functions/make';

import IProductDetailRes from '../../../../share_types/response/user/IProductDetailRes';
import IProductRes from '../../../../share_types/response/user/IProductRes';
import URLS from '../../../../statics/URLS';
import storage from '../../../../storage';

import styles from './ProductWrap.module.scss';

export interface IProductWrapProps {
    product?: IProductRes;
    productDetail?: IProductDetailRes;
}

export default function ProductWrap(props: IProductWrapProps) {
    const messageBox = useMessageBox();
    const router = useRouter();
    const { store, dispatchStore, actions } = storage.cart.useCartStore();

    const productId = props.product?._id;
    const productName = props.product?.title;

    const quantityInCart = React.useMemo(() => {
        if (!productId) return -1; // server error

        const itemInfoInCart = store.data.get(productId);
        if (!itemInfoInCart) return -1; // not added to cart yet

        return itemInfoInCart.quantity;
    }, [productId, store.data]);

    const handlerButtonAddToCartClick = () => {
        if (!productId) return;
        dispatchStore(actions.addToCart(productId));
        messageBox.show({
            title: 'Thông báo',
            message: `Bạn đã thêm ${productName} vào giỏ hàng thành công !`,
            icon: 'info',
            buttons: [
                {
                    label: 'Ok',
                },
            ],
        });
    };

    const handlerButtonBuyNowClick = () => {
        if (!productId) {
            messageBox.show({
                title: 'Lỗi',
                message: `Có lỗi từ máy chủ về sản phẩm này!. Bạn không thể mua sản phẩm này`,
                icon: 'error',
                buttons: [
                    {
                        label: 'Ok',
                    },
                ],
            });
            return;
        }

        if (store.data.has(productId)) {
            router.push(URLS.cart);
            return;
        }

        dispatchStore(actions.addToCart(productId));
        router.push(URLS.cart);
    };

    const handlerQuantityChange = (v: number) => {
        if (!productId) return;
        dispatchStore(actions.changeQuantity(productId, v));
    };

    return (
        <div className={make.className([styles['product-wrap']])}>
            <ImagesProductSlider className={styles['images-slider']} images={props.productDetail?.images} />
            <ProductDetail
                className={styles['detail']}
                title={props.product?.title}
                price={props.product?.price}
                isPromotion={props.product?.isPromotion}
                promotionPrice={props.product?.promotionPrice}
                information={props.productDetail?.information}
                promotion={props.productDetail?.promotion}
                quantityItemInCart={quantityInCart}
                onButtonAddToCartClick={handlerButtonAddToCartClick}
                onButtonBuyNowButtonClick={handlerButtonBuyNowClick}
                onQuantityChange={handlerQuantityChange}
            />
            <div className={styles['article-container']}>
                <aside className={styles['specification']}>
                    <ProductSpecifications className={styles['elmnt']} data={props.productDetail?.specifications} />
                </aside>
                <aside className={styles['article']}>
                    <h2>Thông tin sản phẩm</h2>
                    <Article data={props.productDetail?.description} />
                </aside>
            </div>
        </div>
    );
}
