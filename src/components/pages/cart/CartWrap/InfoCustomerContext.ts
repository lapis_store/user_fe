import { type } from 'os';
import * as React from 'react';

export enum EFields {
    name = 'name',
    phoneNumber = 'phoneNumber',
    address = 'address',
}

export type TInfoCustomerField = {
    isValid: boolean;
    value: string;
    message?: string;
};

export type TInfoCustomer = {
    [property in EFields]: TInfoCustomerField;
};

const InfoCustomerContext = React.createContext<{
    customerInfo: TInfoCustomer;
    setCustomerInfo: React.Dispatch<React.SetStateAction<TInfoCustomer>>;
}>({} as any);

export default InfoCustomerContext;
