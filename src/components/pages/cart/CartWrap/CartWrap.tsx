import * as React from 'react';
import ListProductsInCart from './Components/ListProductsInCart';

import styles from './CartWrap.module.scss';
import InfoCustomer from './Components/InfoCustomer';
import Total from './Components/Total';
import storage from '../../../../storage';
import IProductRes from '../../../../share_types/response/user/IProductRes';
import api from '../../../../api';
import calc from '../../../../functions/calc';
import InfoCustomerContext, { TInfoCustomer } from './InfoCustomerContext';
import IOrderFormData, { IOrderItem } from '../../../../share_types/form_data/user/IOrderFormData';
import useMessageBox from '../../../../hooks/useMessageBox';
import { useRouter } from 'next/router';

export interface ICartWrapProps {}

export default function CartWrap(props: ICartWrapProps) {
    // console.log('[CartWrap] Rerender');
    const { store, dispatchStore, actions } = storage.cart.useCartStore();
    const messageBox = useMessageBox();
    const [mapProducts, setMapProducts] = React.useState<Map<string, IProductRes>>(new Map());
    const router = useRouter();

    const strListId = JSON.stringify(Array.from(store.data.keys()));
    const [customerInfo, setCustomerInfo] = React.useState<TInfoCustomer>({
        name: {
            isValid: false,
            value: '',
        },
        address: {
            isValid: false,
            value: '',
        },
        phoneNumber: {
            isValid: false,
            value: '',
        },
    });
    // const []

    const loadProducts = async (listId: string[]) => {
        const res = await api.product.getByListId(listId);
        if (!res) {
            return;
        }

        setMapProducts(
            res.reduce((total, currentValue) => {
                total.set(currentValue._id, currentValue);
                return total;
            }, new Map<string, IProductRes>()),
        );
    };

    const totalMoney: number = React.useMemo(() => {
        if (!mapProducts) return 0;
        let total = 0;

        for (let [key, value] of store.data) {
            const item = mapProducts.get(key);

            const price = calc.currentProductPrice(item);
            const quantity = value.quantity;

            total += price * quantity;
        }
        return total;
    }, [mapProducts, store.data]);

    const handlerPlaceOrderButtonClick = async () => {
        if (store.data.size === 0) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'error',
                message:
                    'Bạn chưa thêm bất kỳ đơn hàng nào vào giỏ hàng. Hãy thêm một vài thứ vào giỏ hàng rồi đặt hàng.',
                buttons: [
                    {
                        label: 'Ok',
                        callback: () => {
                            console.log('Đã kích hoạt event');
                        },
                    },
                ],
            });
            return;
        }

        const { name, address, phoneNumber } = customerInfo;

        for (let item of Object.values(customerInfo)) {
            if (item.isValid) continue;

            messageBox.show({
                title: 'Thông báo',
                icon: 'error',
                message: item.message || 'Có một vài mục trong phần thông tin khách hàng bạn cần hoàn thiện',
                buttons: [
                    {
                        label: 'Ok',
                    },
                ],
            });
            return;
        }

        const orderItems: IOrderItem[] = [];
        for (let [key, value] of store.data) {
            orderItems.push({
                productId: key,
                quantity: value.quantity,
            });
        }

        const data: IOrderFormData = {
            customerName: name.value,
            address: address.value,
            phoneNumber: phoneNumber.value,
            orderItems,
        };

        const resData = await api.order.add(data);
        if (!resData) {
            messageBox.show({
                title: 'Lỗi quá trình đặt hàng',
                icon: 'error',
                message: 'Có một vài lỗi không mong muốn trong quá trình đặt hàng',
                buttons: [
                    {
                        label: 'Ok',
                    },
                ],
            });
            return;
        }

        messageBox.show({
            title: 'Thành công',
            icon: 'done_all',
            message: 'Đặt hàng thành công',
            buttons: [
                {
                    label: 'Ok',
                },
            ],
        });

        dispatchStore(actions.removeAll());
        router.push('/');
        return;
    };

    React.useEffect(() => {
        loadProducts(JSON.parse(strListId));
        console.log('update');
    }, [strListId]);

    return (
        <div className={styles['cart-wrap']}>
            <div>
                <ListProductsInCart className={styles['list-products-in-cart']} mapProducts={mapProducts} />
                <InfoCustomerContext.Provider value={{ customerInfo, setCustomerInfo }}>
                    <InfoCustomer className={styles['list-customer']} />
                </InfoCustomerContext.Provider>
                <Total
                    className={styles['total']}
                    total={totalMoney}
                    onPlaceOrderButtonClick={handlerPlaceOrderButtonClick}
                />
                {/* <div className={styles['info-customer-and-total']}>
                    </div> */}
            </div>
        </div>
    );
}
