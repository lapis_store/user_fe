import React, { Component } from 'react';
import InfoCustomerContext, { EFields } from '../../../InfoCustomerContext';
import Input, { TInputState } from './Input';

interface IInputAddressProps {}

interface IInputNameState {}

class InputAddress extends Input<IInputAddressProps, IInputNameState> {
    public constructor(props: IInputAddressProps) {
        super(props, {
            helperText: 'Địa chỉ nhận hàng là bắt buộc',
        });

        this.fieldName = EFields.address;
        this.title = 'Địa chỉ nhận hàng';
    }

    protected validator: (v: string) => Promise<{ status: number; message?: string | undefined }> = async (v) => {
        if (v.length === 0) {
            return {
                status: -1,
                message: 'Địa chỉ nhận hàng không được để trống',
            };
        }

        if (v.length < 10) {
            return {
                status: -1,
                message: 'Địa chỉ nhà không hợp lệ',
            };
        }

        return {
            status: 1,
        };
    };

    protected preProcessValue = (v: string): string => {
        // remove extra spaces
        return v.replace(/\s+/g, ' ').trim();
    };
}

export default InputAddress;
