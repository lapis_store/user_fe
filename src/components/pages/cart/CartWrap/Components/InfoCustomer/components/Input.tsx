import * as React from 'react';
import LapisInput from '../../../../../../../share/components/LapisInput';
import useDispatchStop from '../../../../../../../share/hooks/useDispatchStop';
import InfoCustomerContext, { EFields, TInfoCustomerField } from '../../../InfoCustomerContext';

export type TInputProps = {
    className?: string;
};

export type TInputState = {
    displayIcon: boolean;
    value?: string;
    helperText: string;
};

export default class Input<P, S> extends React.Component<TInputProps & P, TInputState> {
    protected defaultValue?: string = '';
    protected title: string = '';
    protected fieldName?: EFields;

    // helperText
    protected get helperText(): string {
        return this.state.helperText;
    }

    protected set helperText(v: string) {
        if (this.state.helperText === v) return;
        this.setState({
            helperText: v,
        });
    }

    // displayIcon
    protected get displayIcon() {
        return this.state.displayIcon;
    }

    protected set displayIcon(v: boolean) {
        if (this.state.displayIcon === v) return;
        this.setState({
            displayIcon: v,
        });
    }

    //
    public declare context: React.ContextType<typeof InfoCustomerContext>;
    public static contextType = InfoCustomerContext;

    protected constructor(props: TInputProps & P, initState: S | (S & TInputState)) {
        super(props);

        this.state = {
            displayIcon: true,
            helperText: '',
            ...initState,
        };
    }

    protected validator = async (
        v: string,
    ): Promise<{
        status: number;
        message?: string;
    }> => {
        return {
            status: -1,
        };
    };

    private validate = async (v: string) => {
        if (!this.fieldName) return;

        const { status, message } = await this.validator(v);

        this.helperText = message || '';
        this.displayIcon = status <= 0;

        this.context.setCustomerInfo((preState) => {
            if (!this.fieldName) return preState;

            const newState: any = {};
            newState[this.fieldName] = {
                ...preState[this.fieldName],
                isValid: status > 0,
                message: message,
            } as TInfoCustomerField;

            return {
                ...preState,
                ...newState,
            };
        });
    };

    protected preProcessValue = (v: string): string => {
        return v;
    };

    private updateValue = (v: string) => {
        if (!this.fieldName) return;

        this.context.setCustomerInfo((preState) => {
            if (!this.fieldName) return preState;

            const oldField = preState[this.fieldName];
            const newState: any = {};

            newState[this.fieldName] = {
                ...oldField,
                value: v,
            } as TInfoCustomerField;

            return {
                ...preState,
                ...newState,
            };
        });
    };

    private handlerInputStopTyping = useDispatchStop((v: string) => {
        const value = this.preProcessValue(v);

        this.updateValue(value);
        this.validate(value);
    }, 1000);

    private handlerChange = (v: string) => {
        this.handlerInputStopTyping(v);
    };

    public render() {
        return (
            <LapisInput
                className={this.props.className}
                title={this.title}
                displayIcon={this.state.displayIcon}
                helperIconColor='red'
                helperIcon='error'
                helperText={this.helperText}
                defaultValue={this.defaultValue}
                onChange={this.handlerChange}
            />
        );
    }
}
