import React, { Component } from 'react';
import InfoCustomerContext, { EFields } from '../../../InfoCustomerContext';
import Input, { TInputState } from './Input';

interface IInputPhoneNumberProps {}

interface IInputPhoneNumberState {}

class InputPhoneNumber extends Input<IInputPhoneNumberProps, IInputPhoneNumberState> {
    public constructor(props: IInputPhoneNumberProps) {
        super(props, {
            helperText: 'Số điện thoại nhập vào là bắt buộc',
        });

        this.fieldName = EFields.phoneNumber;
        this.title = 'Số điện thoại của bạn';
    }

    protected validator: (v: string) => Promise<{ status: number; message?: string | undefined }> = async (v) => {
        if (v.length === 0) {
            return {
                status: -1,
                message: 'Số điện thoại không được để trống',
            };
        }

        const checkPhoneNumber = /^\d{10}$/i;

        if (!checkPhoneNumber.test(v)) {
            return {
                status: -1,
                message: 'Số điện thoại không hợp lệ',
            };
        }

        return {
            status: 1,
        };
    };

    protected preProcessValue = (v: string): string => {
        return v.replace(/\s+/g, ' ').trim();
    };
}

export default InputPhoneNumber;
