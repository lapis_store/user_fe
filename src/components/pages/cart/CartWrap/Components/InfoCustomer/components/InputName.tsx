import React, { Component } from 'react';
import InfoCustomerContext, { EFields } from '../../../InfoCustomerContext';
import Input, { TInputState } from './Input';

interface IInputNameProps {}

interface IInputNameState {}

class InputName extends Input<IInputNameProps, IInputNameState> {
    public constructor(props: IInputNameProps) {
        super(props, {
            helperText: 'Họ và tên là bắt buộc',
        });

        this.fieldName = EFields.name;
        this.title = 'Họ và tên';
    }

    protected validator: (v: string) => Promise<{ status: number; message?: string | undefined }> = async (
        v: string,
    ) => {
        if (v.length === 0) {
            return {
                status: -1,
                message: 'Họ và tên là bắt buộc',
            };
        }

        return {
            status: 1,
        };
    };

    protected preProcessValue = (v: string): string => {
        return v.replace(/\s+/g, ' ').trim();
    };
}

export default InputName;
