import * as React from 'react';
import LapisInput from '../../../../../../share/components/LapisInput';
import make from '../../../../../../share/functions/make';
import InfoCustomerContext from '../../InfoCustomerContext';
import InputAddress from './components/InputAddress';
import InputName from './components/InputName';
import InputPhoneNumber from './components/InputPhoneNumber';

import styles from './InfoCustomer.module.scss';

export interface IInfoCustomerProps {
    className?: string;
}

export default function InfoCustomer(props: IInfoCustomerProps) {
    // const {customerInfo, setCustomerInfo} = React.useContext(customerInfoContext);

    // console.log(customerInfo);

    const handlerChange = (v: string) => {
        console.log(v);
    };

    return (
        <div className={make.className([styles['info-customer'], props.className])}>
            <h2 id='#thong-tin-khach-hang'>Thông tin khách hàng</h2>
            <div className={styles['container']}>
                <InputName className={styles['input']} />
                <InputPhoneNumber className={styles['input']} />
                <InputAddress className={styles['input']} />
            </div>
        </div>
    );
}
