/* eslint-disable @next/next/no-img-element */
import Link from 'next/link';
import * as React from 'react';
import QuantityInput from '../../../../../../../share/components/QuantityInput';
import make from '../../../../../../../share/functions/make';

import IProductRes from '../../../../../../../share_types/response/user/IProductRes';
// import QuantityInput from '../../../../../../reuse/QuantityInput';

import styles from './Item.module.scss';

export interface IItemProps {
    className?: string;
    data?: IProductRes;
    quantity?: number;

    onQuantityChange?: (v: number) => any;
    onButtonAddOneClick?: () => any;
    onButtonRemoveOneClick?: () => any;
    onButtonRemoveClick?: () => any;
}

export default function Item(props: IItemProps) {
    const data = props.data;

    if (!data || props.quantity === undefined || props.quantity < 1) {
        return <div className={make.className([styles['item'], props.className])}>Có lỗi trong quá trình hiển thị</div>;
    }

    function formatPrice(v: number): string {
        return v.toLocaleString('vi-VN');
    }

    const renderPrice = () => {
        if (!data.isPromotion || !data.promotionPrice)
            return (
                <div className={styles['price']}>
                    <strong>{formatPrice(data.price)}</strong>
                </div>
            );

        return (
            <div className={styles['price']}>
                <strong>{formatPrice(data.promotionPrice)}</strong>
                <span>{formatPrice(data.price)}</span>
                <i>Giảm {Math.round(100 - (data.promotionPrice / data.price) * 100)}%</i>
            </div>
        );
    };

    return (
        <div className={make.className([styles['item'], props.className])}>
            <div className={styles['image']}>
                <img src={make.imageAddress(data.image)} alt={data.title} />
            </div>

            <div className={styles['info']}>
                <div className={styles['title']}>
                    <Link href={make.linkProduct(props.data?.slug)} passHref>
                        <a>{data.title}</a>
                    </Link>
                </div>

                {renderPrice()}

                <div className={styles['quantity-container']}>
                    <label>Số lượng</label>
                    <QuantityInput
                        className={styles['input-quantity']}
                        min={1}
                        defaultValue={props.quantity}
                        onChange={props.onQuantityChange}
                        onButtonNextClick={props.onButtonAddOneClick}
                        onButtonPrevClick={props.onButtonRemoveOneClick}
                    />
                </div>

                <div className={styles['buttons']}>
                    <button className={'remove'} onClick={props.onButtonRemoveClick}>
                        Xoá khỏi giỏi hàng
                    </button>
                </div>
            </div>
        </div>
    );
}
