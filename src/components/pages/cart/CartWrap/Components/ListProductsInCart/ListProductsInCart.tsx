import * as React from 'react';
import api from '../../../../../../api';
import IProductRes from '../../../../../../share_types/response/user/IProductRes';
import storage from '../../../../../../storage';

import styles from './ListProductsInCart.module.scss';
import Item from './Item';
import make from '../../../../../../share/functions/make';

export interface IListProductsInCartProps {
    className?: string;
    mapProducts: Map<string, IProductRes>;
}

function ListProductsInCart(props: IListProductsInCartProps) {
    console.info('[ListProductsInCart] Rerender');
    const { store, dispatchStore } = storage.cart.useCartStore();

    const handlerQuantityChange = (id: string) => (quantity: number) => {
        dispatchStore(storage.cart.actions.changeQuantity(id, quantity));
    };

    const handlerButtonAddOneClick = (id: string) => () => {
        dispatchStore(storage.cart.actions.addToCart(id));
    };

    const handlerButtonRemoveOneClick = (id: string) => () => {
        dispatchStore(storage.cart.actions.removeFromCart(id));
    };

    const handlerButtonRemoveClick = (id: string) => () => {
        dispatchStore(storage.cart.actions.changeQuantity(id, -1));
    };

    const cartItems: {
        id: string;
        quantity: number;
        createdAt: Date;
    }[] = React.useMemo(() => {
        const _cartItems: {
            id: string;
            quantity: number;
            createdAt: Date;
        }[] = [];

        for (let [id, data] of store.data) {
            _cartItems.push({
                ...data,
                id,
            });
        }

        _cartItems.sort((a, b) => {
            if (a.createdAt > b.createdAt) return -1;
            if (a.createdAt < b.createdAt) return 1;
            return 0;
        });

        return _cartItems;
    }, [store.data]);

    const renderItemsElmnts = () => {
        if (cartItems.length === 0) return <strong>Không có sản phẩm nào trong giỏ hàng của bạn</strong>;

        return cartItems.map((elmnt) => {
            return (
                <Item
                    key={`${elmnt.id}`}
                    data={props.mapProducts.get(elmnt.id)}
                    quantity={elmnt.quantity}
                    onQuantityChange={handlerQuantityChange(elmnt.id)}
                    onButtonAddOneClick={handlerButtonAddOneClick(elmnt.id)}
                    onButtonRemoveOneClick={handlerButtonRemoveOneClick(elmnt.id)}
                    onButtonRemoveClick={handlerButtonRemoveClick(elmnt.id)}
                />
            );
        });
    };

    return <div className={make.className([styles['cart'], props.className])}>{renderItemsElmnts()}</div>;
}

export default React.memo(ListProductsInCart);
