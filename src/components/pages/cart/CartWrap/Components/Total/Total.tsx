import * as React from 'react';
import make from '../../../../../../share/functions/make';

import styles from './Total.module.scss';

export interface ITotalProps {
    className?: string;
    total?: number;
    onPlaceOrderButtonClick?: () => any;
}

export default function Total(props: ITotalProps) {
    return (
        <div className={make.className([styles['total'], props.className])}>
            <div>
                <span>Tổng tiền tạm tính: </span>
                <strong>{(props.total || 0).toLocaleString('vn-VN')}</strong>
            </div>
            <button onClick={props.onPlaceOrderButtonClick}>Đặt hàng</button>
        </div>
    );
}
