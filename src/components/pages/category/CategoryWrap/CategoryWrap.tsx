import * as React from 'react';
import calc from '../../../../functions/calc';
import LapisBaseSelection, { IOption } from '../../../../share/components/LapisBaseSelection';
import ICategoryRes from '../../../../share_types/response/user/category/ICategoryRes';
import IProductRes from '../../../../share_types/response/user/IProductRes';
import GroupProducts from '../../../reuse/GroupProducts';

import styles from './CategoryWrap.module.scss';

export interface ICategoryWrapProps {
    data?: IProductRes[];
    category?: ICategoryRes;
}

const sortByOptions: IOption[] = [
    {
        label: 'Nổi bật',
        value: 'noi-bat',
    },
    {
        label: 'Giá thấp đến cao',
        value: 'gia-thap-den-cao',
    },
    {
        label: 'Giá cao đến thấp',
        value: 'gia-cao-den-thap',
    },
];

export default function CategoryWrap(props: ICategoryWrapProps) {
    const [sortBy, setSortBy] = React.useState<string>(sortByOptions[0].value);

    const currentProducts = React.useMemo(() => {
        if (!props.data) return [];

        if (sortBy === 'noi-bat') return props.data;

        const nSortBy = sortBy === 'gia-thap-den-cao' ? 1 : -1;

        const _currentProducts = [...props.data];
        _currentProducts.sort((a, b) => {
            const aCurrentPrice = calc.currentProductPrice(a);
            const bCurrentPrice = calc.currentProductPrice(b);

            if (aCurrentPrice > bCurrentPrice) return 1 * nSortBy;
            if (aCurrentPrice < bCurrentPrice) return -1 * nSortBy;
            return 0;
        });
        return _currentProducts;
    }, [sortBy, props.data]);

    const handlerSelected = (v: IOption) => {
        setSortBy(v.value);
    };

    return (
        <div className={styles['category-wrap']}>
            <div className={styles['header']}>
                <h1>{props.category?.title}</h1>
                <div className={styles['right']}>
                    <label>Sắp xếp theo</label>
                    <LapisBaseSelection
                        className={styles['selection']}
                        options={sortByOptions}
                        selectedValue={sortBy}
                        onSelected={handlerSelected}
                    />
                </div>
            </div>
            {currentProducts.length === 0 ? (
                <div>Không có sản phẩm nào để hiển thị</div>
            ) : (
                <GroupProducts data={currentProducts} />
            )}
        </div>
    );
}
