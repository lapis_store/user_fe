import * as React from 'react';

import styles from './NotFoundWrap.module.scss';

export interface INotFoundWrapProps {}

export default function NotFoundWrap(props: INotFoundWrapProps) {
    return (
        <div className={styles['not-found']}>
            <div>
                <strong>404</strong>
                <span>Xin lỗi, chúng tôi không tìm thấy trang mà bạn cần!</span>
            </div>
        </div>
    );
}
