import * as React from 'react';
import Link from 'next/link';

import api from '../../../api';

import styles from './Footer.module.scss';
import converts from '../../../share/functions/converts';
import Container from '../../reuse/Container';
import make from '../../../share/functions/make';
import IFooterResponse from '../../../share_types/response/user/IFooterResponse';

export interface IFooterProps {}

function Footer(props: IFooterProps) {
    const [data, setData] = React.useState<IFooterResponse[] | undefined>(undefined);

    const loadData = async () => {
        const res = await api.getFooter();
        setData(res);
    };

    const footerItems =
        data &&
        data.map((item, i) => {
            const titleElmnt = !item.title ? undefined : (
                <div className={styles.title}>
                    {item.title}
                    <div className={styles.underline}></div>
                </div>
            );

            const descriptionElmnt = !item.description ? undefined : (
                <p className={styles.description}>{item.description}</p>
            );

            const linksElmnts =
                !item.links || item.links.length === 0 ? undefined : (
                    <ul className={styles.links}>
                        {item.links.map((liItem, liIndex) => {
                            return (
                                <li key={`link-${liIndex}`}>
                                    <Link href={liItem.url}>
                                        <a>
                                            <span>{liItem.title}</span>
                                        </a>
                                    </Link>
                                </li>
                            );
                        })}
                    </ul>
                );

            const iconsElmnts =
                !item.icons || item.icons.length === 0 ? undefined : (
                    <ul className={styles.icons}>
                        {item.icons.map((liItem, liIndex) => {
                            return (
                                <li key={`icon-${liIndex}`}>
                                    <Link href={liItem.url}>{converts.numericToUnicode(liItem.name)}</Link>
                                </li>
                            );
                        })}
                    </ul>
                );

            return (
                <div
                    key={i}
                    className={make.className([styles.item, styles[item.width], item.align && styles[item.align]])}
                >
                    {titleElmnt}
                    {descriptionElmnt}
                    {linksElmnts}
                    {iconsElmnts}
                </div>
            );
        });

    React.useEffect(() => {
        loadData();
    }, []);

    return (
        <footer className={make.className([styles.footer])}>
            <Container className={make.className([styles.wrap])}>{footerItems}</Container>
        </footer>
    );
}

export default React.memo(Footer);
