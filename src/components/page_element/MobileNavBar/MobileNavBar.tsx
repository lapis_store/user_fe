import Link from 'next/link';
import * as React from 'react';
import make from '../../../share/functions/make';
import useRootStore from '../../../storage/root/useRootStore';

import styles from './MobileNavBar.module.scss';

export interface IMobileNavBarProps {}

export default function MobileNavBar(props: IMobileNavBarProps) {
    const [pageName, setPageName] = React.useState<string>('');

    const { store, dispatchStore, storeActions } = useRootStore();

    const handlerCategoryClick = () => {
        dispatchStore(storeActions.setDisplayCategory(!store.displayCategory));
    };

    React.useEffect(() => {
        if (typeof window === 'undefined') return;

        const regex = new RegExp('https?://[^/]+/([^/]*)/?', 'i');
        const matchResult = window.location.href.match(regex);
        if (!matchResult) return;
        setPageName(matchResult[1]);
    }, []);

    return (
        <div className={styles['mobile-nav-bar']}>
            <button
                className={make.className([store.displayCategory && 'selected'], styles)}
                onClick={handlerCategoryClick}
            >
                <span>dns</span>
                <span>Danh mục</span>
            </button>
            <Link href={'/'} passHref>
                <a
                    className={make.className(
                        [
                            // 'selected'
                            pageName === '' && 'selected',
                        ],
                        styles,
                    )}
                >
                    <span>home</span>
                    <span>Trang chủ</span>
                </a>
            </Link>
            <Link href={`/cart`} passHref>
                <a
                    className={make.className(
                        [
                            // 'selected'
                            pageName === 'cart' && 'selected',
                        ],
                        styles,
                    )}
                >
                    <span>shopping_cart</span>
                    <span>Giỏ hàng</span>
                </a>
            </Link>
        </div>
    );
}
