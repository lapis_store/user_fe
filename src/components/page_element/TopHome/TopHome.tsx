import * as React from 'react';
import make from '../../../share/functions/make';
import IHomeResponse from '../../../share_types/response/home/IHomeResponse';
import Carousel from '../../reuse/Carousel';
import Container from '../../reuse/Container';
import GroupBanner from '../../reuse/GroupBanner';

import styles from './TopHome.module.scss';

export interface ITopHomeProps {
    homeData?: IHomeResponse | null;
}

export default function TopHome(props: ITopHomeProps) {
    return (
        <div className={make.className([styles['top-home']])}>
            <Container className={styles['container']}>
                <Carousel className={styles['carousel']} data={props.homeData?.carousel} />
                <GroupBanner className={styles['group-banner']} data={props.homeData?.bannersRightCarousel} />
            </Container>
        </div>
    );
}
