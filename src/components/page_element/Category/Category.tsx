import * as React from 'react';

import api from '../../../api';

import { TCategoriesRes } from '../../../api/category/list';
import LapisCover from '../../../share/components/LapisCover';
import make from '../../../share/functions/make';
import useRootStore from '../../../storage/root/useRootStore';
import Container from '../../reuse/Container';

import styles from './Category.module.scss';
import CategoryItemLevel1 from './components/CategoryItemLevel1';
import ICategoryItem from './types/ICategoryItem';

export interface ICategoriesProps {}

export default function Category(props: ICategoriesProps) {
    const [categories, setCategories] = React.useState<TCategoriesRes | undefined>();

    const { store, dispatchStore, storeActions } = useRootStore();

    const loadCategories = async () => {
        const res = await api.category.list();
        setCategories(res);
    };

    const handlerCoverClick = () => {
        dispatchStore(storeActions.setDisplayCategory(false));
    };

    const categoryTreeObject: ICategoryItem[] = React.useMemo(() => {
        const makeCategoryTreeObject = (parentId: string | undefined = undefined, added: Set<string> = new Set()) => {
            if (!categories) return [];

            const items: ICategoryItem[] = categories
                .filter((item) => {
                    if (item.parentId !== parentId) return false;
                    if (added.has(item._id)) return false;
                    return true;
                })
                .map((item) => ({
                    ...item,
                    children: makeCategoryTreeObject(item._id, added),
                }));

            items.sort((a, b) => {
                if (a.index > b.index) return 1;
                if (a.index < b.index) return -1;

                if (a.createdAt > b.createdAt) return -1;
                if (a.createdAt < b.createdAt) return 1;

                return 0;
            });

            return items;
        };

        return makeCategoryTreeObject();
    }, [categories]);

    const categoryItemsElmnts = React.useMemo(() => {
        return categoryTreeObject.map((item) => {
            return <CategoryItemLevel1 key={item._id} data={item} />;
        });
    }, [categoryTreeObject]);

    React.useEffect(() => {
        loadCategories();
    }, []);

    return (
        <div className={make.className([styles['category']])}>
            <LapisCover className={styles['cover']} display={store.displayCategory} onClick={handlerCoverClick} />
            <Container className={make.className(['container', store.displayCategory && 'display'], styles)}>
                {categoryItemsElmnts}
            </Container>
        </div>
    );
}
