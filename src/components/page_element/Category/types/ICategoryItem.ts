import ICategoryRes from '../../../../share_types/response/user/category/ICategoryRes';

interface ICategoryItem extends ICategoryRes {
    children: ICategoryItem[];
}

export default ICategoryItem;
