import Link from 'next/link';
import * as React from 'react';
import EPages from '../../../../../core/EPages';
import ICategoryItem from '../../types/ICategoryItem';
import optimizeKeyword from '../../../../../share/functions/optimizeKeyword';

import styles from './CategoryItemLevelN.module.scss';

export interface ICategoryItemLevelNProps {
    data: ICategoryItem;
}

export default function CategoryItemLevelN(props: ICategoryItemLevelNProps) {
    const childrenElmnt = (() => {
        if (props.data.children.length === 0) return null;

        const liElmnts = props.data.children.map((child) => {
            return (
                <li key={child._id}>
                    <CategoryItemLevelN data={child} />
                </li>
            );
        });

        return (
            <div className={styles['wrap']}>
                <ul>{liElmnts}</ul>
            </div>
        );
    })();

    const slug = optimizeKeyword(props.data.title).replace(/[\s]+/g, '-').trim();

    return (
        <div className={styles['category-item-level-n']}>
            <Link href={`/${EPages.category}/${slug}?id=${props.data._id}`} passHref>
                <a className={styles['label']}>{props.data.title}</a>
            </Link>
            {childrenElmnt}
        </div>
    );
}
