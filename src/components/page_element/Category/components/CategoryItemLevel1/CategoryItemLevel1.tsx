import Link from 'next/link';
import * as React from 'react';
import EPages from '../../../../../core/EPages';
import make from '../../../../../share/functions/make';
import optimizeKeyword from '../../../../../share/functions/optimizeKeyword';
import ICategoryItem from '../../types/ICategoryItem';
import CategoryItemLevelN from '../CategoryItemLevelN';

import styles from './CategoryItemLevel1.module.scss';

export interface ICategoryItemLevel1Props {
    data: ICategoryItem;
}

export default function CategoryItemLevel1(props: ICategoryItemLevel1Props) {
    const childrenElmnt = (() => {
        if (props.data.children.length === 0) return null;

        const liElmnts = props.data.children.map((child) => {
            return (
                <li key={child._id}>
                    <CategoryItemLevelN data={child} />
                </li>
            );
        });

        return (
            <div className={styles['wrap']}>
                <ul>{liElmnts}</ul>
            </div>
        );
    })();

    const slug = optimizeKeyword(props.data.title).replace(/[\s]+/g, '-').trim();

    return (
        <div className={styles['category-item-level1']}>
            <Link href={`/${EPages.category}/${slug}?id=${props.data._id}`} passHref>
                <a>
                    <div
                        className={styles['image']}
                        style={{
                            backgroundImage: `url('${make.imageAddress(props.data.image, 'thumbnail')}')`,
                        }}
                    />

                    <div className={styles['label']}>{props.data.title}</div>
                </a>
            </Link>

            {childrenElmnt}
        </div>
    );
}
