import * as React from 'react';
import MessageBox from '../../../share/components/MessageBox';
import make from '../../../share/functions/make';
import storage from '../../../storage';

import styles from './ListMessageBox.module.scss';

export interface IListMessageBoxProps {}

export default function ListMessageBox(props: IListMessageBoxProps) {
    const { store, dispatchStore, storeActions } = storage.root.useRootStore();

    const handlerMessageBoxClose = (id: number) => () => {
        dispatchStore(storeActions.removeMessageAlert(id));
    };

    const renderListMessageBox = () => {
        const listMessageBoxElmnts: JSX.Element[] = [];

        for (let [key, value] of store.messageBoxData) {
            listMessageBoxElmnts.push(
                <MessageBox
                    key={key}
                    title={value.title}
                    className={styles['message-box']}
                    icon={value.icon}
                    message={value.message}
                    buttons={value.buttons}
                    onClose={handlerMessageBoxClose(key)}
                />,
            );
        }
        return listMessageBoxElmnts;
    };

    return <div className={make.className([styles['list-message-box']])}>{renderListMessageBox()}</div>;
}
