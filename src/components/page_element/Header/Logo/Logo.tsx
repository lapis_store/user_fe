import * as React from 'react';

import styles from './Logo.module.scss';
import Link from 'next/link';
import make from '../../../../share/functions/make';
import ENV_SHARE from '../../../../share/core/ENV_SHARE';

export interface ILogoProps {
    hide?: boolean;
}

export default function Logo(props: ILogoProps) {
    return (
        <Link href='/' passHref>
            <a className={make.className([styles.logo, props.hide && styles['hide']])}>
                <div className={styles['logo-image-wrap']}>
                    <div className={styles['logo-image']}>storefront</div>
                </div>
                <div className={styles['web-name']}>{ENV_SHARE.WEB_NAME}</div>
            </a>
        </Link>
    );
}
