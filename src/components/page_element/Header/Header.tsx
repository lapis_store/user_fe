import * as React from 'react';

import LapisSearch from '../../../share/components/LapisSearch/LapisSearch';

import Logo from './Logo';
import Buttons from './Buttons';

import styles from './Header.module.scss';
import { ISuggestion } from '../../../share/components/LapisSearch';
import api from '../../../api';
import { useRouter } from 'next/router';
import Container from '../../reuse/Container';
import make from '../../../share/functions/make';

const suggestion: ISuggestion[] = [
    {
        href: '#',
        text: 'Khẩu trang y tế',
    },
    {
        href: '#',
        text: 'Khẩu trang vải',
    },
    {
        href: '#',
        text: 'Khẩu trang y tế',
    },
    {
        href: '#',
        text: 'Khẩu trang y tế',
    },
    {
        href: '#',
        text: 'Khẩu trang y tế',
    },
];

export interface IHeaderProps {}

export default function Header(props: IHeaderProps) {
    const [searchFocus, setSearchFocus] = React.useState<boolean>(false);
    const router = useRouter();

    const handlerSearchFocus = React.useCallback(() => {
        setSearchFocus(true);
    }, []);

    const handlerSearchBlur = React.useCallback(() => {
        setSearchFocus(false);
    }, []);

    const handlerGetSuggestion = React.useCallback(async (v: string): Promise<ISuggestion[] | undefined> => {
        const suggestions = await api.search.suggestion(v);
        if (!suggestions) return undefined;

        return suggestions.map((item) => ({
            text: item,
            href: `/search?keyword=${item}`,
        }));
    }, []);

    const handlerSearch = React.useCallback(
        (v: string) => {
            router.push(`/search?keyword=${v}`);
        },
        [router],
    );

    return (
        <header className={make.className([styles.header, searchFocus && styles['search-focus']])}>
            <div className={styles['wrap']}>
                <Container className={make.className([styles['container']])}>
                    <Logo hide={searchFocus} />
                    <LapisSearch
                        className={styles['search']}
                        suggestions={suggestion}
                        suggestionTitle={'Có phải bạn muốn tìm'}
                        onFocus={handlerSearchFocus}
                        onBlur={handlerSearchBlur}
                        onGetSuggestions={handlerGetSuggestion}
                        onSearch={handlerSearch}
                    />
                    <Buttons />
                </Container>
            </div>
        </header>
    );
}
