import * as React from 'react';
import make from '../../../../share/functions/make';

import AccountButton from './AccountButton';

import styles from './Buttons.module.scss';
import CartButton from './CartButton';

export interface IButtonsProps {}

export default function Buttons(props: IButtonsProps) {
    return (
        <div className={make.className([styles['buttons']])}>
            <CartButton className={styles['button']} />
            <AccountButton className={styles['button']} />
        </div>
    );
}
