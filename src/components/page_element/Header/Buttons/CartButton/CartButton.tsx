import * as React from 'react';

import CartPreview from './CartPreview/CartPreview';
import URLS from '../../../../../statics/URLS';
import storage from '../../../../../storage';
import Link from 'next/link';

import styles from './CartButton.module.scss';
import make from '../../../../../share/functions/make';

export interface ICartButtonProps {
    className?: string;
}

export default function CartButton(props: ICartButtonProps) {
    const [displayCartPreview, setDisplayCartPreview] = React.useState<boolean>(false);
    const { store, dispatchStore } = storage.cart.useCartStore();

    const handlerMouseEnter = () => {
        setDisplayCartPreview(true);
    };

    const handlerMouseLeave = () => {
        setDisplayCartPreview(false);
    };

    return (
        <div
            className={make.className([styles['cart-button'], props.className])}
            onMouseEnter={handlerMouseEnter}
            onMouseLeave={handlerMouseLeave}
        >
            <Link href={URLS.cart} passHref>
                <a className={styles['icon']}>
                    <strong>shopping_cart</strong>
                    <span data-count={store.data.size}></span>
                </a>
            </Link>
            <CartPreview display={displayCartPreview} />
        </div>
    );
}
