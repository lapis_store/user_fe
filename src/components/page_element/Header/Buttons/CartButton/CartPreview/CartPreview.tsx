import * as React from 'react';

import Item from './Item';

import styles from './CartPreview.module.scss';
import URLS from '../../../../../../statics/URLS';
import storage from '../../../../../../storage';
import IProductRes from '../../../../../../share_types/response/user/IProductRes';
import api from '../../../../../../api';
import calc from '../../../../../../functions/calc';
import useCartStore from '../../../../../../storage/cart/useCartStore';
import make from '../../../../../../share/functions/make';
import LapisLink from '../../../../../../flex/LapisLink';

export interface ICartPreviewProps {
    display?: boolean;
}

export default function CartPreview(props: ICartPreviewProps) {
    const { store, dispatchStore, actions } = storage.cart.useCartStore();

    const [mapProducts, setMapProducts] = React.useState<Map<string, IProductRes>>(new Map());

    const strListId = JSON.stringify(Array.from(store.data.keys()));

    const loadMapProducts = async (listId: string[]) => {
        const products = await api.product.getByListId(listId);

        if (!products) return;

        setMapProducts(() => {
            return products.reduce((newMapProduct, item) => {
                newMapProduct.set(item._id, item);
                return newMapProduct;
            }, new Map<string, IProductRes>());
        });
    };

    const handlerButtonRemoveClick = React.useCallback(
        (id: string) => {
            // dispatch quantity <= 0 is remove product;
            dispatchStore(actions.changeQuantity(id, 0));
        },
        [dispatchStore, actions],
    );

    React.useEffect(() => {
        loadMapProducts(JSON.parse(strListId));
    }, [strListId]);

    const itemsElmnts = React.useMemo(() => {
        const cartItems: {
            id: string;
            quantity: number;
            createdAt: Date;
        }[] = [];

        for (let [key, value] of store.data) {
            const currentProduct = mapProducts.get(key);
            if (!currentProduct) continue;

            cartItems.push({
                id: key,
                quantity: value.quantity,
                createdAt: value.createdAt,
            });
        }

        cartItems.sort((a, b) => {
            if (a.createdAt > b.createdAt) return -1;
            if (a.createdAt < b.createdAt) return 1;
            return 0;
        });

        return cartItems.map((item) => {
            const currentProduct = mapProducts.get(item.id);
            if (!currentProduct) return undefined;

            return (
                <Item
                    className={styles['item']}
                    key={`${item.id}`}
                    id={item.id}
                    image={currentProduct.image}
                    title={currentProduct.title}
                    price={calc.currentProductPrice(currentProduct)}
                    url={currentProduct.slug}
                    quantity={item.quantity}
                    onButtonRemoveClick={handlerButtonRemoveClick}
                />
            );
        });
    }, [store.data, mapProducts, handlerButtonRemoveClick]);

    return (
        <div className={make.className([styles['cart-preview-wrap'], props.display && styles['display']])}>
            <div className={styles['container']}>
                <div className={styles['cart-preview']}>
                    <div className={styles['title']}>Đơn hàng đã thêm gần đây</div>
                    <div className={styles['items']}>{itemsElmnts}</div>
                    <div className={styles['buttons']}>
                        <LapisLink href={URLS.cart}>Xem giỏ hàng</LapisLink>
                    </div>
                </div>
            </div>
        </div>
    );
}
