/* eslint-disable @next/next/no-img-element */
import * as React from 'react';

import styles from './Item.module.scss';
import Link from 'next/link';
import make from '../../../../../../../share/functions/make';

export interface IItemProps {
    className?: string;

    id: string;
    url: string;
    title: string;
    price: number;
    image: string;
    quantity: number;

    onButtonRemoveClick?: (id: string) => any;
}

export default function Item(props: IItemProps) {
    return (
        <div className={make.className([styles['item'], props.className])}>
            <div className={styles['image']} style={{ backgroundImage: `url('${make.imageAddress(props.image)}')` }} />
            <div className={styles['container']}>
                <Link href={props.url || '#'} passHref>
                    <a className={styles['title']}>{props.title}</a>
                </Link>
                <div className={styles['price']}>
                    {(props.price || 0).toLocaleString('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                    })}
                </div>
                <div className={styles['quantity']}>
                    <span>Số lượng: </span>
                    <span>{props.quantity}</span>
                </div>
            </div>
            <button
                onClick={() => {
                    if (props.onButtonRemoveClick) props.onButtonRemoveClick(props.id);
                }}
            />
        </div>
    );
}
