import * as React from 'react';

import styles from './AccountButton.module.scss';
import URLS from '../../../../../statics/URLS';
import make from '../../../../../share/functions/make';
import LapisLink from '../../../../../flex/LapisLink';

export interface IAccountButtonProps {
    className?: string;
}

export default function AccountButton(props: IAccountButtonProps) {
    return (
        <div
            className={make.className([styles['account-button'], props.className])}
            style={
                {
                    '--number-of-buttons': '2',
                } as any
            }
        >
            <LapisLink className={styles['icon']} href={'#'}>
                person
            </LapisLink>
            <div className={styles['wrap']}>
                <div className={styles['container']}>
                    <div className={styles['window']}>
                        <LapisLink href={'#'} className={styles['button']}>
                            Đăng xuất
                        </LapisLink>
                        <LapisLink href={URLS.login} className={styles['button']}>
                            Đăng nhập
                        </LapisLink>
                    </div>
                </div>
            </div>
        </div>
    );
}
