import * as React from 'react';
import make from '../../../share/functions/make';
import IProductRes from '../../../share_types/response/user/IProductRes';

import ListProducts from '../ListProducts/ListProducts';
import styles from './GroupProducts.module.scss';

export interface IGroupProductsProps {
    className?: string;
    data?: IProductRes[];
}

export default function GroupProducts(props: IGroupProductsProps) {
    return (
        <div className={make.className([styles['group-products'], props.className])}>
            <ListProducts data={props.data} className={styles['product-card']} />
        </div>
    );
}
