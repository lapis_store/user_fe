import * as React from 'react';
import make from '../../../share/functions/make';
import IBanner from '../../../share_types/others/IBanner';

import styles from './Carousel.module.scss';

// const data = [
//     'https://cdn2.cellphones.com.vn/690x300/https://dashboard.cellphones.com.vn/storage/tn690-300-max%20(2).png',
//     'https://cdn2.cellphones.com.vn/690x300/https://dashboard.cellphones.com.vn/storage/xiaomi_p1.png',
//     'https://cdn2.cellphones.com.vn/690x300/https://dashboard.cellphones.com.vn/storage/note-11.png',
//     'https://cdn2.cellphones.com.vn/690x300/https://dashboard.cellphones.com.vn/storage/1000xm5.png',
//     'https://cdn2.cellphones.com.vn/690x300/https://dashboard.cellphones.com.vn/storage/matebook-d15.jpg',
// ];

export interface ICarouselProps {
    data?: IBanner[];
    intervalDelay?: number;
    className?: string;
}

export default function Carousel(props: ICarouselProps) {
    const timerIdRef = React.useRef<NodeJS.Timer | undefined>(undefined);
    const isHover = React.useRef<boolean>(false);

    const [position, setPosition] = React.useState<number>(0);
    const [direction, setDirection] = React.useState<'to-left' | 'to-right' | undefined>();

    const data = React.useMemo(() => {
        return props.data || [];
    }, [props.data]);

    const nextPosition = (() => {
        if (position + 1 >= data.length) return 0;
        return position + 1;
    })();

    const prePosition = (() => {
        if (position - 1 < 0) return data.length - 1;
        return position - 1;
    })();

    const makePositionClassName = (v: number) => {
        // v is index of list images
        if (v === position) return styles['center'];
        if (v === prePosition) return styles['left'];
        if (v === nextPosition) return styles['right'];
        return undefined;
    };

    const moveTo = (v: number) => {
        setDirection(undefined);
        setPosition(v);
    };

    const next = () => {
        setDirection('to-right');
        setPosition(nextPosition);
    };

    const pre = () => {
        setDirection('to-left');
        setPosition(prePosition);
    };

    // event handler

    const handlerButtonClick = (buttonType: 'left' | 'right') => () => {
        if (data.length === 0) return;

        if (buttonType === 'left') {
            pre();
            return;
        }

        // right
        next();
    };

    const handlerIndicationClick = (index: number) => () => {
        moveTo(index);
    };

    // pending loop when hovering
    const handlerMouseEnter = () => {
        isHover.current = true;
    };

    const handlerMouseLeave = () => {
        isHover.current = false;
    };

    React.useEffect(() => {
        let _intervalDelay = props.intervalDelay || 3000;

        if (_intervalDelay < 1000) _intervalDelay = 1000;

        timerIdRef.current = setInterval(() => {
            if (isHover.current) return;

            setDirection('to-right');
            setPosition((preState) => {
                if (preState + 1 >= data.length) return 0;
                return preState + 1;
            });
        }, _intervalDelay);
        // console.log('set');

        return () => {
            if (!timerIdRef.current) return;

            clearInterval(timerIdRef.current);
            // console.log('clear');
        };
    }, [props.intervalDelay, data.length]);

    const renderItemsElmnt = () => {
        return data.map((banner, i) => {
            const { image, alt, url } = banner;

            return (
                <div key={i} className={make.className([styles['layer-wrap'], makePositionClassName(i)])}>
                    <div className={styles['layer']}>
                        <div
                            className={styles['image']}
                            style={{
                                backgroundImage: `url('${make.imageAddress(image, 'medium')}')`,
                            }}
                        ></div>
                    </div>
                </div>
            );
        });
    };

    const renderIndicatorsElmnt = () => {
        const items: JSX.Element[] = [];
        for (let i = 0; i < data.length; i++) {
            const isCenter = i === position;
            items.push(
                <li
                    key={i}
                    className={make.className([isCenter && styles['center']])}
                    onClick={handlerIndicationClick(i)}
                />,
            );
        }
        return items;
    };

    return (
        <div
            className={make.className([styles['carousel'], props.className, direction && styles[direction]])}
            onMouseEnter={handlerMouseEnter}
            onMouseLeave={handlerMouseLeave}
        >
            <div className={styles['buttons-wrap']}>
                <div className={styles['buttons-container']}>
                    <button onClick={handlerButtonClick('left')}>
                        <svg viewBox='0 0 16 16' aria-hidden='true'>
                            <path d='M9.573 4.427L6.177 7.823a.25.25 0 000 .354l3.396 3.396a.25.25 0 00.427-.177V4.604a.25.25 0 00-.427-.177z'></path>
                        </svg>
                    </button>
                    <button onClick={handlerButtonClick('right')}>
                        <svg viewBox='0 0 16 16' aria-hidden='true'>
                            <path d='M6.427 4.427l3.396 3.396a.25.25 0 010 .354l-3.396 3.396A.25.25 0 016 11.396V4.604a.25.25 0 01.427-.177z'></path>
                        </svg>
                    </button>
                </div>
            </div>
            <div className={styles['layers-container']}>{renderItemsElmnt()}</div>
            <div className={styles['indicators-container']}>
                <ul>{renderIndicatorsElmnt()}</ul>
            </div>
        </div>
    );
}
