import * as React from 'react';
import make from '../../../share/functions/make';

import IProductRes from '../../../share_types/response/user/IProductRes';
import ListProducts from '../ListProducts/ListProducts';

import styles from './ListProductsSlider.module.scss';

export type TDataType = IProductRes[];

export interface IListProductsSliderProps {
    className?: string;
    data?: TDataType;
}

export default function ListProductsSlider(props: IListProductsSliderProps) {
    const listProductsElmntRef = React.useRef<HTMLDivElement>(null);
    const productsWrapElmntRef = React.useRef<HTMLDivElement>(null);

    const [pageNumber, setPageNumber] = React.useState<number>(0);

    const numberOfItems = props.data?.length || 0;

    // if click button left then v = -1 else v = 1
    const handlerButtonClick = (v: 1 | -1) => () => {
        if (!listProductsElmntRef.current) return;
        if (!productsWrapElmntRef.current) return;

        const cs = getComputedStyle(listProductsElmntRef.current);

        if (cs.getPropertyValue('--is-extra-small-device')) {
            const productsWrapWidth = productsWrapElmntRef.current.clientWidth;
            const currentPageNumber = Math.floor(productsWrapElmntRef.current.scrollLeft / productsWrapWidth);

            productsWrapElmntRef.current.scrollLeft = (currentPageNumber + v) * productsWrapWidth + 1;
            return;
        }

        productsWrapElmntRef.current.scrollLeft = 0;

        setPageNumber((preState) => {
            const numberOfItemsInView = parseInt(cs.getPropertyValue('--number-of-items-in-view'));
            const maxPageNumber =
                numberOfItems % numberOfItemsInView === 0
                    ? numberOfItems / numberOfItemsInView
                    : Math.floor(numberOfItems / numberOfItemsInView) + 1;

            if (preState >= maxPageNumber) preState = maxPageNumber - 1;
            const newState = preState + v;

            if (newState < 0) return maxPageNumber - 1;
            if (newState >= maxPageNumber) return 0;
            return newState;
        });
    };

    return (
        <div
            ref={listProductsElmntRef}
            className={make.className([styles['list-products-slider'], props.className])}
            style={
                {
                    '--number-of-items': numberOfItems,
                    '--page-number': pageNumber,
                } as any
            }
        >
            <div className={styles['buttons-wrap']}>
                <div className={styles['buttons-container']}>
                    <button onClick={handlerButtonClick(-1)}>{String.fromCharCode(8249)}</button>
                    <button onClick={handlerButtonClick(1)}>{String.fromCharCode(8250)}</button>
                </div>
            </div>
            <div ref={productsWrapElmntRef} className={styles['products-wrap']}>
                <div className={styles['products-container']}>
                    <ListProducts data={props.data} className={styles['product-card']} />
                </div>
            </div>
        </div>
    );
}
