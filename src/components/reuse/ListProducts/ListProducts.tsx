import * as React from 'react';

import ProductCardWithEventHandler from '../../../containers/ProductCardWithEventHandler';
import IProductRes from '../../../share_types/response/user/IProductRes';

export interface IListProductsProps {
    className?: string;
    data?: IProductRes[];
}

export default function ListProducts(props: IListProductsProps) {
    const renderProductsElmnts = props.data?.map((item, i) => {
        return (
            <ProductCardWithEventHandler
                key={i}
                className={props.className}
                id={item._id}
                slug={item.slug}
                title={item.title}
                isPromotionalPrice={item.isPromotion}
                price={item.price}
                promotionPrice={item.promotionPrice}
                image={item.image}
                summary={item.summary}
            />
        );
    });

    return <>{renderProductsElmnts}</>;
}
