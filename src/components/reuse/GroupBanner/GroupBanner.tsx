import * as React from 'react';
import make from '../../../share/functions/make';
import IBanner from '../../../share_types/others/IBanner';
import Banner, { IBannerProps } from '../Banner';

import styles from './GroupBanner.module.scss';

const data: IBannerProps[] = [
    {
        images: 'https://cdn2.cellphones.com.vn/690x300/https://dashboard.cellphones.com.vn/storage/RB_galaxy-A73.jpg',
    },
    {
        images: 'https://cdn2.cellphones.com.vn/690x300/https://dashboard.cellphones.com.vn/storage/Right%20banner.png',
    },
    {
        images: 'https://cdn2.cellphones.com.vn/690x300/https://dashboard.cellphones.com.vn/storage/idepad_3.png',
    },
];

export interface IGroupBannerProps {
    data?: IBanner[];
    className?: string;
}

export default function GroupBanner(props: IGroupBannerProps) {
    const renderBannersElmnt = () => {
        if (!props.data) return null;

        return props.data.map((item, i) => {
            return <Banner key={`${i}_${item.image}`} images={item.image} alt={item.alt} href={item.url} />;
        });
    };

    return <div className={make.className([styles['group-banner'], props.className])}>{renderBannersElmnt()}</div>;
}
