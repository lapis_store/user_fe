/* eslint-disable @next/next/no-img-element */
import * as React from 'react';
import make from '../../../share/functions/make';
import banner from './banner.png';

import ListProductsSlider, { TDataType } from '../ListProductsSlider';

import styles from './HighlightListProductsSlider.module.scss';

export interface IHighlightListProductsSliderProps {
    className?: string;
    data?: TDataType;
    image?: string;
    backgroundColor?: string;
}

export default function HighlightListProductsSlider(props: IHighlightListProductsSliderProps) {
    return (
        <div
            className={make.className([styles['highlight-list-products-slider'], props.className])}
            style={{ backgroundColor: props.backgroundColor }}
        >
            <img src={banner.src} alt='alt' />
            <ListProductsSlider data={props.data} />
        </div>
    );
}
