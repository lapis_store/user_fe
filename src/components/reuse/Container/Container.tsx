import * as React from 'react';
import make from '../../../share/functions/make';

import styles from './Container.module.scss';

export interface IContainerProps {
    className?: string;
}

export default function Container(props: React.PropsWithChildren<IContainerProps>) {
    return <div className={make.className([styles.container, props.className])}>{props.children}</div>;
}
