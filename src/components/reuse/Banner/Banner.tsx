/* eslint-disable @next/next/no-img-element */
import Link from 'next/link';
import * as React from 'react';
import make from '../../../share/functions/make';

import styles from './Banner.module.scss';

export interface IImages {
    maxWidth: number; // max-width = 0 => default
    src: string;
}

export interface IBannerProps {
    className?: string;
    href?: string;
    images?: string | IImages[];
    alt?: string;
}

export default function Banner(props: IBannerProps) {
    const renderImagesElmnt = () => {
        if (typeof props.images === 'string') {
            return (
                <picture>
                    <source media={'(min-width: 1000px)'} srcSet={make.imageAddress(props.images, 'large')} />
                    <source media={'(min-width: 600px)'} srcSet={make.imageAddress(props.images, 'medium')} />
                    <img alt={props.alt} src={make.imageAddress(props.images, 'small')} />
                </picture>
            );
        }

        if (Array.isArray(props.images)) {
            const images = [...props.images];

            images.sort((a, b) => {
                if (a.maxWidth === 0) return 1;
                if (b.maxWidth === 0) return -1;

                if (a.maxWidth > b.maxWidth) return 1;
                if (a.maxWidth < b.maxWidth) return -1;

                return 0;
            });

            return (
                <picture>
                    {images.map((item, i) => {
                        const media = item.maxWidth === 0 ? undefined : `(max-width: ${item.maxWidth}px)`;
                        return <source key={`${i}_${item.src}`} media={media} srcSet={item.src} />;
                    })}
                    <img alt={props.alt} />
                </picture>
            );
        }
    };

    return (
        <div className={make.className([styles['banner'], props.className])}>
            <Link href={props.href || '#'} passHref>
                <a>{renderImagesElmnt()}</a>
            </Link>
        </div>
    );
}
