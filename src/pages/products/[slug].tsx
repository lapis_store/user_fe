import { GetServerSideProps } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import * as React from 'react';
import api from '../../api';
import ProductWrap from '../../components/pages/product/ProductWrap';
import useMessageBox from '../../hooks/useMessageBox';
import DefaultLayout from '../../layouts/DefaultLayout';
import ProductDetail from '../../share/components/ProductDetail';
import make from '../../share/functions/make';

import IProductDetailRes from '../../share_types/response/user/IProductDetailRes';
import IProductRes from '../../share_types/response/user/IProductRes';
import storage from '../../storage';

export interface IProductProps {
    product?: IProductRes;
    productDetail?: IProductDetailRes;
    slug?: string;
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const { slug } = context.query;

    const { product, productDetail } = await api.product.getBySlug(slug);

    return {
        props: {
            product,
            productDetail,
            slug,
        } as IProductProps,
    };
};

export default function Product(props: IProductProps) {
    const router = useRouter();

    return (
        <>
            <Head>
                <title>{props.product?.title}</title>
            </Head>

            <div className={make.className(['page', 'product-page'])}>
                <DefaultLayout>
                    <ProductWrap product={props.product} productDetail={props.productDetail} />
                </DefaultLayout>
            </div>
        </>
    );
}
