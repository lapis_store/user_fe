import { GetServerSideProps } from 'next';
import Head from 'next/head';
import * as React from 'react';
import api from '../../api';
import CategoryWrap from '../../components/pages/category/CategoryWrap';
import GroupProducts from '../../components/reuse/GroupProducts';

import DefaultLayout from '../../layouts/DefaultLayout';
import make from '../../share/functions/make';
import ICategoryRes from '../../share_types/response/user/category/ICategoryRes';
import IProductRes from '../../share_types/response/user/IProductRes';

export interface ICategoryProps {
    products?: IProductRes[];
    category?: ICategoryRes;
}

export const getServerSideProps: GetServerSideProps = async (
    context,
): Promise<{
    props: ICategoryProps;
}> => {
    const { id } = context.query;
    if (typeof id !== 'string') {
        return {
            props: {},
        };
    }

    return {
        props: {
            products: await api.product.list(id),
            category: await api.category.find(id),
        },
    };
};

export default function Category(props: ICategoryProps) {
    return (
        <>
            <Head>
                <title>{props.category?.title}</title>
            </Head>

            <div className={make.className(['page', 'product-page'])}>
                <DefaultLayout>
                    <CategoryWrap category={props.category} data={props.products} />
                </DefaultLayout>
            </div>
        </>
    );
}
