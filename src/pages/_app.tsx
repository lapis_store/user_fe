import '../styles/globals.scss';
import type { AppProps } from 'next/app';
import storage from '../storage';
import CartFromLocalStorage from '../containers/CartFromLocalStorage';
import ListMessageBox from '../components/page_element/ListMessageBox';

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <storage.root.Provider>
            <storage.cart.Provider>
                <CartFromLocalStorage>
                    <Component {...pageProps} />
                </CartFromLocalStorage>
            </storage.cart.Provider>
            <ListMessageBox />
        </storage.root.Provider>
    );
}

export default MyApp;
