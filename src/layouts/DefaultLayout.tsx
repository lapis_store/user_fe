import * as React from 'react';
import Category from '../components/page_element/Category';
import Footer from '../components/page_element/Footer';
import Header from '../components/page_element/Header';
import MobileNavBar from '../components/page_element/MobileNavBar';
import Container from '../components/reuse/Container';

export interface IDefaultLayoutProps {}

export default function DefaultLayout(props: React.PropsWithChildren<IDefaultLayoutProps>) {
    return (
        <>
            <Header />
            <Category />
            <main>
                <Container>{props.children}</Container>
            </main>
            <Footer />
            <MobileNavBar />
        </>
    );
}
