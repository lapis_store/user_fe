import * as React from 'react';
import Category from '../components/page_element/Category';
import Footer from '../components/page_element/Footer';
import Header from '../components/page_element/Header';
import MobileNavBar from '../components/page_element/MobileNavBar';
import TopHome from '../components/page_element/TopHome';
import Banner from '../components/reuse/Banner';
import Container from '../components/reuse/Container';

import ProductCard, { IProductCardProps } from '../share/components/ProductCard';
import IHomeResponse from '../share_types/response/home/IHomeResponse';

export interface IHomeLayoutProps {
    homeData?: IHomeResponse | null;
}

export default function HomeLayout(props: React.PropsWithChildren<IHomeLayoutProps>) {
    return (
        <>
            <Header />
            <Category />
            <TopHome homeData={props.homeData} />

            <Container>
                <Banner
                    images={props.homeData?.bannersBelowCarousel[0].image}
                    alt={props.homeData?.bannersBelowCarousel[0].alt}
                    href={props.homeData?.bannersBelowCarousel[0].url}
                />
            </Container>

            <Container>{props.children}</Container>
            <Footer />
            <MobileNavBar />
        </>
    );
}
