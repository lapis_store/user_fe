import Link from 'next/link';
import * as React from 'react';

export interface ILinkProps {
    className?: string;
    href: string;
    style?: React.CSSProperties;
    onClick?: () => any;
}

export default function LapisLink(props: React.PropsWithChildren<ILinkProps>) {
    return (
        <Link href={props.href} onClick={props.onClick} passHref>
            <a className={props.className} style={props.style}>
                {props.children}
            </a>
        </Link>
    );
}
