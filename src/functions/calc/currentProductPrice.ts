import IProductRes from '../../share_types/response/user/IProductRes';

const currentProductPrice = (p: IProductRes | undefined) => {
    if (!p) return 0;

    if (!p.isPromotion || p.promotionPrice === undefined) {
        return p.price;
    }

    return p.promotionPrice;
};

export default currentProductPrice;
