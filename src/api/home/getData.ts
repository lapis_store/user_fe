import axios from 'axios';
import ENV from '../../config/STATIC_VARS';
import IHomeResponse from '../../share_types/response/home/IHomeResponse';

const url = [ENV.API_HOST, 'static', 'data', 'home.json'].join('/');

const getData = async (): Promise<IHomeResponse | undefined> => {
    try {
        const res = await axios.get(url);
        if (res.status !== 200) return undefined;
        return res.data;
    } catch {
        return undefined;
    }
};

export default getData;
