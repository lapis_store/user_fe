import axios from 'axios';
import ENV from '../config/STATIC_VARS';

import IFooterResponse from '../share_types/response/user/IFooterResponse';

const url = [ENV.API_HOST, 'user', 'footer', 'list'].join('/');

export async function getFooter(): Promise<IFooterResponse[] | undefined> {
    try {
        const res = await axios.get(url);

        if (res.status !== 200) {
            console.log(`[WARNING] Can't load footer-items`);
            return undefined;
        }

        const footerItems: IFooterResponse[] = res.data;
        return footerItems;
    } catch {
        return undefined;
    }
}
