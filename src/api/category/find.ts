import axios from 'axios';
import ENV from '../../config/STATIC_VARS';
import ICategoryRes from '../../share_types/response/user/category/ICategoryRes';

const find = async (id: string): Promise<ICategoryRes | undefined> => {
    const url: string = [ENV.API_HOST, 'user', 'categories', 'find', id].join('/');

    try {
        const res = await axios.get(url);

        if (res.status !== 200) {
            console.error(`Can't get categories from ${url}`);
            return undefined;
        }

        return res.data;
    } catch {
        console.error(`Error get categories from ${url}`);
        return undefined;
    }
};

export default find;
