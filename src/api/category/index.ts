import find from './find';
import list from './list';

const categories = {
    find,
    list,
};

export default categories;
