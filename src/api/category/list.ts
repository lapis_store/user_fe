import axios from 'axios';
import ENV from '../../config/STATIC_VARS';
import ICategoryRes from '../../share_types/response/user/category/ICategoryRes';

export type TCategoriesRes = ICategoryRes[];

const list = async (): Promise<TCategoriesRes> => {
    const url: string = `${ENV.API_HOST}/user/categories/list`;

    try {
        const res = await axios.get(url);

        if (res.status !== 200) {
            console.error(`Can't get categories from ${url}`);
            return [];
        }

        const categories: TCategoriesRes = res.data;
        return categories;
    } catch {
        console.error(`Error get categories from ${url}`);
        return [];
    }
};

export default list;
