import axios from 'axios';
import ENV from '../../config/STATIC_VARS';
import IOrderFormData from '../../share_types/form_data/user/IOrderFormData';
import IAddOrderRes from '../../share_types/response/user/order/IAddOrderRes';

const add = async (data: IOrderFormData): Promise<IAddOrderRes | undefined> => {
    const url = `${ENV.API_HOST}/user/order/add`;

    try {
        const res = await axios.post(url, {
            ...data,
        });

        if (res.status !== 201) return undefined;

        const resData: IAddOrderRes = res.data;
        return resData;
    } catch {
        return undefined;
    }
};

export default add;
