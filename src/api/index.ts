import categories from './category';
import { getFooter } from './getFooter';
import home from './home';
import order from './order';
import product from './product';
import search from './search';

const api = {
    home,
    product,
    getFooter,
    category: categories,
    order,
    search,
};

export default api;
