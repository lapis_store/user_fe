import getByListId from './getByListId';
import getBySlug from './getBySlug';
import latestProducts from './latestProducts';
import list from './list';

const product = {
    list,
    getBySlug,
    getByListId,
    latestProducts,
};

export default product;
