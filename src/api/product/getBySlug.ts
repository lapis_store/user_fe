import axios from 'axios';
import ENV from '../../config/STATIC_VARS';
import IProductDetailRes from '../../share_types/response/user/IProductDetailRes';

import IProductRes from '../../share_types/response/user/IProductRes';

const regexSlug = /^[a-zA-Z0-9\-]+$/;

const getBySlug = async (
    slug?: string | string[],
): Promise<{ product?: IProductRes; productDetail?: IProductDetailRes }> => {
    if (!slug || typeof slug !== 'string') return {};
    if (!regexSlug.test(slug)) return {};

    const uri = axios.getUri({
        url: `${ENV.API_HOST}/user/product/get-by-slug`,
        params: {
            slug,
        },
    });

    try {
        const res = await axios.get(uri);

        if (res.status !== 200) {
            return {};
        }

        // const {product, productDetail} = res.data;
        return res.data;
    } catch {
        return {};
    }
};

export default getBySlug;
