import axios from 'axios';
import ENV from '../../config/STATIC_VARS';

import IProductRes from '../../share_types/response/user/IProductRes';

const latestProducts = async (): Promise<IProductRes[] | undefined> => {
    const url = `${ENV.API_HOST}/user/product/latest-products`;
    try {
        const res = await axios.get(url);
        if (res.status !== 200) {
            return undefined;
        }

        const newProducts: IProductRes[] = res.data;
        return newProducts;
    } catch {
        return undefined;
    }
};

export default latestProducts;
