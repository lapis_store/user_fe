import axios from 'axios';
import ENV from '../../config/STATIC_VARS';
import IProductRes from '../../share_types/response/user/IProductRes';

const url: string = [ENV.API_HOST, 'user', 'product', 'list'].join('/');

async function list(categoryId?: string): Promise<IProductRes[] | undefined> {
    try {
        const res = await axios.get(url, {
            params: {
                categoryId,
            },
        });

        if (res.status !== 200) return undefined;

        if (!Array.isArray(res.data)) return undefined;

        const productsResponse: IProductRes[] = res.data;
        return productsResponse;
    } catch {
        return undefined;
    }
}

export default list;
