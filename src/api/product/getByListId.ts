import axios from 'axios';
import ENV from '../../config/STATIC_VARS';

import IProductRes from '../../share_types/response/user/IProductRes';

const latestRes: {
    uri: string;
    data: any;
} = {
    uri: '',
    data: undefined,
};

const getByListId = async (listId: string[]): Promise<IProductRes[] | undefined> => {
    if (listId.length === 0) return undefined;

    const uri = axios.getUri({
        url: `${ENV.API_HOST}/user/product/get-by-list-id`,
        params: {
            listId,
        },
    });

    if (uri === latestRes.uri) {
        console.log('[api.getByListId] use cache');
        const newProducts: IProductRes[] = structuredClone(latestRes.data);
        return newProducts;
    }

    try {
        const res = await axios.get(uri);

        if (res.status !== 200) {
            return undefined;
        }

        // save cache
        latestRes.uri = uri;
        latestRes.data = res.data;

        const newProducts: IProductRes[] = structuredClone(res.data);
        return newProducts;
    } catch {
        return undefined;
    }
};

export default getByListId;
