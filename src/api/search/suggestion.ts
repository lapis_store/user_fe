import axios from 'axios';
import ENV from '../../config/STATIC_VARS';

const suggestion = async (q: string): Promise<string[] | undefined> => {
    if (q.length === 0) return undefined;

    const url = `${ENV.API_HOST}/user/search/suggestion`;
    try {
        const res = await axios.get(url, {
            params: {
                q,
            },
        });

        if (res.status !== 200) return undefined;

        const suggestions: string[] = res.data;
        return suggestions;
    } catch {
        return undefined;
    }
};

export default suggestion;
