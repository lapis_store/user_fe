import axios from 'axios';
import ENV from '../../config/STATIC_VARS';
import IProductRes from '../../share_types/response/user/IProductRes';

const findOut = async (q: string): Promise<IProductRes[] | undefined> => {
    if (q.length === 0) return undefined;

    const url = `${ENV.API_HOST}/user/search`;
    try {
        const res = await axios.get(url, {
            params: {
                keyword: q,
            },
        });

        if (res.status !== 200) return undefined;

        const products: IProductRes[] = res.data;
        return products;
    } catch {
        return undefined;
    }
};

export default findOut;
