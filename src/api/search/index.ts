import findOut from './findOut';
import suggestion from './suggestion';
const search = {
    findOut,
    suggestion,
};

export default search;
