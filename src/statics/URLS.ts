class URLS {
    public static readonly cart: string = '/cart';
    public static readonly login: string = '/login';
}

export default URLS;
